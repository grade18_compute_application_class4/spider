'use strict'
// 创建数据库的表

const Sequelize = require('sequelize');
const sequelize = require('../db');
//标题、作者、发布时间，有三个字段
const article = sequelize.define('article', {
    title: {
        type: Sequelize.STRING(100),
        allowNull: false
    },
    author: {
        type: Sequelize.STRING(80),
    },
    releasetime: {//datetimeoffset(7)
        type: Sequelize.DATE
    }

});

// 暴露创建的表
module.exports=article;