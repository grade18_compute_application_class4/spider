'use strict'

// var User = require('./models/articles')
var addTable = require('./utils/initDb')

addTable();

module.exports = () => {
    article.sync({
        force: true
    })
}

//爬虫
var articles = require('./models/articles')
var Crewler = require('crawler');

var c = new Crewler({
    maxConnections: 10,
    //这将为每个爬取的页面调用 
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            var $ = res.$;

            var list = $('#content .article table').toArray();

            list.forEach(element => {
                //add title
                var title = $(element).find(' .pl2 a').text().trim().replace(/[ \r\n]/g, '');
                console.log(title);

                //add writer
                var writer = $(element).find(' .pl').text().trim().replace(/[ \r\n]/g, '');
                console.log(writer);

                //add introduce
                var introdtion = $(element).find(' span.inq').text().trim().replace(/[ \r\n]/g, '');
                console.log(introdtion);

                //add all
                articles.create({ title: title, writer: writer, introdtion: introdtion })

            })
        }
        done();
    }
});

// Queue just one URL, with default callback
// 队列只有一个url，默认回调 
c.queue('https://book.douban.com/top250?icn=index-book250-all')


// User.create({ firstName: "Jane", lastName: "Doe" }).then(jane => {
//     console.log("Jane's auto-generated ID:", jane.id);
// });