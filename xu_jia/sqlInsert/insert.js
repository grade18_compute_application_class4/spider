'use strict'

var table = require('../createTable/table')

module.exports = () => {
    table.sync({ force: true })
};
