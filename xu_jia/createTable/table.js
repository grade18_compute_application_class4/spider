'use strict'

var Sequelize = require('sequelize');
var sequelize = require('../db');

const table = sequelize.define('article', {

  txtname: {
    type: Sequelize.STRING(4000),
    allowNull: false
  },artname:{
    type: Sequelize.STRING(4000),
    allowNull: false
  },introduced:{
    type: Sequelize.STRING(4000),
    allowNull: false
  },
  picture:{
    type: Sequelize.STRING(4000),
    allowNull: false
  }
}, {

});
module.exports=table;