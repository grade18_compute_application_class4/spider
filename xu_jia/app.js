'use strict'

// // Option 2: Passing parameters separately (other dialects)
// // const sequelize = new Sequelize('database', 'username', 'password', {
// //   host: 'localhost',
// //   dialect: 'mssql'/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
// // });

// // try {
// //     sequelize.authenticate();
// //     console.log('Connection has been established successfully.');
// //   } catch (error) {
// //     console.error('Unable to connect to the database:', error);
// //   }


const fs = require('fs') 
const request = require('request') 
var insert = require('./sqlInsert/insert');
insert();
var table = require('.//createTable/table');

// var User = require('./createTable/table');
// insert();

// setTimeout(() => {
//   User.findAll().then(function(users) {
//     console.log(JSON.stringify(users,null,4))
//   })
//   // User.update({ lastName: "Doe" }, {
//   //   where: {
//   //     lastName: null
//   //   }
//   // }).then(() => {
//   //   console.log("Done");
//   // });
// // User.findAll().then(users => {
// //     console.log("All users:", JSON.stringify(users, null, 4));
// //   });  
// }, 3000);

// setTimeout(() => {
//   User.destroy({
//     where: {
//       firstName:"许"
//     }
//   }).then(() => {
//     console.log("Done");
//   });
// }, 4000);

var Crawler = require("crawler");

var c = new Crawler({
    rateLimit:200 ,
    maxConnections: 1,
    // This will be called for each crawled page
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            var $ = res.$;

            var list = $('#content .article table').toArray();
            list.forEach(element => {
                var txt0 = $(element).find(' a ').text().trim().replace(/\s*/g, "");

                var txt1 = $(element).find(' .pl ').text().trim().replace(/\s*/g, "");
                var a = txt1.split('/').slice(0,1).toString();

                var txt2 = $(element).find(' .inq ').text().trim().replace(/\s*/g, "");

                var txt3 = $(element).find(' img ').attr('src');
                var imgName = txt3.split('/').slice(7,8).toString();
                request(txt3).pipe(fs.createWriteStream('./img/'  + imgName + '.jpg'))

               
                // console.log(txt0);
                // console.log(a);
                // console.log(txt2);
                // console.log(txt3);
                table.create({ txtname: txt0, artname: a, introduced: txt2 ,picture:txt3});
            });
        }
        done();
    }
});

// c.queue(['https://book.douban.com/top250?start=0','https://book.douban.com/top250?start=25','https://book.douban.com/top250?start=50',
// 'https://book.douban.com/top250?start=75','https://book.douban.com/top250?start=100','https://book.douban.com/top250?start=125','https://book.douban.com/top250?start=150',
// 'https://book.douban.com/top250?start=175','https://book.douban.com/top250?start=200','https://book.douban.com/top250?start=225'
// ])

for (let index = 0; index < 250; index += 25) {
    c.queue('https://book.douban.com/top250?start='+index);     
}

// c.queue('https://book.douban.com/top250?start=0')
// c.queue('https://book.douban.com/top250?start=25')
// c.queue('https://book.douban.com/top250?start=50')
// c.queue('https://book.douban.com/top250?start=75')
// c.queue('https://book.douban.com/top250?start=100')
// c.queue('https://book.douban.com/top250?start=125')
// c.queue('https://book.douban.com/top250?start=150')
// c.queue('https://book.douban.com/top250?start=175')
// c.queue('https://book.douban.com/top250?start=200')
// c.queue('https://book.douban.com/top250?start=225')

// c.queue('https://book.douban.com/top250?start=250')