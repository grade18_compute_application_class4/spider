'use strict'



var initDb = require('./utils/initDb');

initDb();

// seq
//   .authenticate()
//   .then(() => {
//     console.log('Connection has been established successfully.');
//   })
//   .catch(err => {
//     console.error('Unable to connect to the database:', err);
//   });
var table = require('./models/article');
var Crawler = require("crawler");
for (let index = 0; index <= 250; index += 25) {
    var URL = 'https://book.douban.com/top250?start=' + index
    var c = new Crawler({
        maxConnections: 5000,
        // This will be called for each crawled page
        callback: function (error, res, done) {
            if (error) {
                console.log(error);
            } else {
                var $ = res.$;
                var list = $('#wrapper .article table').toArray();
                list.forEach(element => {
                    var txt1 = $(element).find('.pl2').text().trim().replace(/[ \n\r]/g, "");
                    var txt2 = $(element).find('.pl').text().trim().replace(/\s*/g, "");
                    var txt3 = $(element).find('.quote').text().trim().replace(/\s*/g, "");



                    var a = txt2.split('/').slice(0, 1).toString();
                    table.create({ title: txt1, who: a, intro: txt3 })
                    console.log(txt1);
                    console.log(txt2);
                    console.log(txt3);
                });
                // $ is Cheerio by default
                //a lean implementation of core jQuery designed specifically for the server


            }
            done();
        }
    });

    c.queue(URL);


}