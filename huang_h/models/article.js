'use strict'

var Sequelize = require('sequelize');
//连接db实例
var sequelize = require('../db');

//
const table = sequelize.define('articles', {
    // attributes
    title: {
      type: Sequelize.STRING(),
      allowNull: false
    },
    who: {
      type: Sequelize.STRING(),
      // allowNull defaults to true
    },
    intro:{
      type: Sequelize.STRING(),
      allowNull: true
    }
    
  }, {
    // options
  });
  module.exports=table;