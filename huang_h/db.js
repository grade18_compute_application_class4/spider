'use strict'
const Sequelize = require('sequelize');
 


// Option 1: Passing parameters separately
const sequelize = new Sequelize('Test', 'sa', '123456', {
  host: 'localhost',
  dialect: 'mssql'/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});



module.exports=sequelize;