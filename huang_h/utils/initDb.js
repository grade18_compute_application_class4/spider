'use strict'

var article = require('../models/article')

module.exports = () => {
    article.sync({ force: true });
}

