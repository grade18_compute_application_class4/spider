'use strict'

const Sequelize = require('sequelize');

const sequelize = require('../db');

const cnblog = sequelize.define('cnblog', {
    // attributes
    title: {
      type: Sequelize.STRING(4000),
      allowNull: false
    },
    author: {
      type: Sequelize.STRING
      // allowNull defaults to true
    },
    newstime: {
        type: Sequelize.STRING
    }
  }, {
    // options
  });

module.exports = cnblog;