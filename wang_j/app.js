'use strict'

const initDb = require('./utils/initDb');

initDb();

const crawler = require('./reptile/crawler');

crawler();