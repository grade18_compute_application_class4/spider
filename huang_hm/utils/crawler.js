'use strict'
var User=require('../models/articles');
var Crawler=require('crawler');

var crawler=new Crawler({
    // 在每个请求处理完毕后将调用此回调函数
    callback: function (error, res, done){
        if(error){
            console.log(error);
        }else{
            //将爬好的数据赋值给juqery对象
            var $=res.$;
            // $ 默认为 Cheerio 解析器
            // 它是核心jQuery的精简实现，可以按照jQuery选择器语法快速提取DOM元素
            console.log($("title").text());//获取标题
            //获取要爬取的节点信息 
            var list = $('#content .article .indent table').toArray(); //转为数组 

            list.forEach(element => {
                //获取书名
                let bookname = $(element).find('.pl2 a').text().trim().replace(/[ \n]/g, '');//替换正则
                //获取作者
                let author = $(element).find('p.pl').text().replace(/([/][^/]+){3,4}$/, '');
                //获取引文
                let quote = $(element).find('span.inq').text();

                User.Book.sync({
                    force: true
                }).then(()=>{
                    User.Book.create({
                        BookName: bookname,
                        Author: author,
                        Quote: quote
                    }).then(row=>{
                        console.log(`所插入数据的ID：${row.id}:${row.BookName}:${row.Author}:${row.Quote}`)

                    });
                })
            });  

            }
        done();
        }
    
});


for (let val = 0; val <= 250; val += 25) {
    let url = 'https://book.douban.com/top250?start=' + val;
    crawler.queue(url);
}