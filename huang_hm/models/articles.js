'use strict'

var Sequelize=require('sequelize');
var sequelize=require('../db');

//这边就相当于创建数据库的表
// const table=sequelize.define('articles',{//define 是sequlize 里面自带一个方法，他有主要两个参数
//     //attributse 属性
//     title:{//这个相当于表里面的字段
//         type:Sequelize.STRING(800),
//         allowNull:false
//     }
// },{
//     //opations 选项
// });

const User=sequelize.define('user',{
    UserName:{
        type:Sequelize.STRING(80),
        allowNull:false
    },

    PassWord:{
        type:Sequelize.STRING(20)
    }
},{
    timestamps:true
});


const Book=sequelize.define('book',{
    BookName:{
        type:Sequelize.STRING,
        allowNull:false
    },

    Author:{
        type:Sequelize.STRING,
        allowNull:false
    },

    Quote:{
        type:Sequelize.STRING,
        allowNull:false
    }
},{
    timestamps:true
})


module.exports={
    User:User,
    Book:Book

}