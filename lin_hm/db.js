//上一次
// 'use strict'

// const Sequelize = require('sequelize');

// const sequelize = new Sequelize('lianxi1', 'sa', '123456', {
//     host: 'localhost',
//     dialect: 'mssql'/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
//   });

// module.exports = sequelize;

'use strict'

const Sequelize = require('sequelize');

const sequelize = new Sequelize('lianxi1', 'sa', '123456', {
    host: 'localhost',
    dialect: 'mssql'/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
  });

module.exports = sequelize;