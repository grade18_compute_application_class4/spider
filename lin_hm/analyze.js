//调用downLoad( )一个一个下载图片
const cheerio = require('cheerio');
const fs = require('fs');
function findImg(dom, Callback, arr) {
    let $ = cheerio.load(dom);
    $('.nbg img').each(function (i, elem) {

        let imgSrc = $(this).attr('src');
        arr.push(imgSrc);
        // console.log(arr.length);
        Callback(imgSrc, arr.length);
    });
}
module.exports.findImg = findImg;