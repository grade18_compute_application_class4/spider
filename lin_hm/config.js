const url = 'https://book.douban.com/top250?start=';

const path = require('path');

const imgDir = path.join(__dirname,'img');

module.exports.url = url;
module.exports.imgDir = imgDir;