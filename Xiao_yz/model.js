const Sequelize = require('sequelize');

// Option 1: Passing parameters separately
const sequelize = new Sequelize('work', 'sa', '123456', {
  host: 'localhost',
  dialect:  'mssql' 
});

// sequelize
//   .authenticate()
//   .then(() => {
//     console.log('Connection has been established successfully.');
//   })
//   .catch(err => {
//     console.error('Unable to connect to the database:', err);
//   });
module.exports=sequelize;