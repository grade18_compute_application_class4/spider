'use strict'

var Sequelize=require('sequelize');

const sequelizeInstance=require('../db'); //实例

var table = sequelizeInstance.define('articles', {
    // attributes
    title: {
      type: Sequelize.STRING(4000),
      allowNull: false
    }
  }, {
    // options
  });

module.exports=table;