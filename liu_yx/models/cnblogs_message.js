var sequelize = require('../db');
var Sequelize = require('sequelize');

const table = sequelize.define('cnblogs', {
    // attributes
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    author: {
        type: Sequelize.STRING,
        allowNull: false
    },
    ReleaseTime: {
        type: Sequelize.STRING,
        allowNull: false
    }
}, {
    sequelize,
    timestamps: false,
    freezeTableName: true
})

module.exports = table;