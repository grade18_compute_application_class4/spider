var sequelize = require('../db');
var Sequelize = require('sequelize');

const table = sequelize.define('articles', {
  // attributes
  title: {
    type: Sequelize.STRING,
    allowNull: false
  },
  author: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Introduction: {
    type: Sequelize.STRING,
    allowNull: false
  },
  imgFilePath: {
    type: Sequelize.STRING,
    allowNull: false
  }
}, {
  // options
});

module.exports = table;