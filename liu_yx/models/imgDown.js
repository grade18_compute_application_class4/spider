const request = require('request');
const fs = require('fs');

function downloadPic(src, dest) {
    request(src).pipe(fs.createWriteStream(dest))
}
module.exports = downloadPic;