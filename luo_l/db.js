'use strict'

const Sequelize = require('sequelize');

const sequelize = new Sequelize('node-mssql', 'sa', '123456', {
  host: 'localhost',
  dialect: 'mssql'/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

module.exports = sequelize;