'use strict';

const Sequelize = require('sequelize');

//// Option 1: Passing parameters separately
const sequelize = new Sequelize('book','sa','123456',{
    host:'localhost',
    dialect:'mssql'/*one of 'mysql' | 'mariabd' | 'pastagres' | 'mssql' */
});
// Option 2: Passing a connection URI
// const sequelize = new Sequelize('postgres://user:pass@example.com:5432/dbname');
//Blog_Garden

//测试
// sequelize
//     .authenticate()
//     .then(()=>{
//         console.log('Connection has been established successfully.');
//     })
//     .catch(err=>{
//         console.log('Unable to connect to the database:',err);
//     });

module.exports = sequelize;