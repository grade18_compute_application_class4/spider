'use strict';

var Sequelize = require('sequelize');
var sequelize = require('../db');
const table = sequelize.define('articles', {
    // attributes
    // title:Sequelize.STRING,
    //Introduction:Sequelize.STRING
    title: {//书
        type: Sequelize.STRING,
        allowNull: false
    },
    author: {//作者
        type: Sequelize.STRING,
        allowNull: false
    },
    Introduction: {//简介
        type: Sequelize.STRING,
        allowNull: false
    }
}, {
    // options
    freezeTableName: true
});


module.exports = table;