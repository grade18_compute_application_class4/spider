//创建链接

'use strict';
const Sequelize = require('sequelize');

// Option 1: Passing parameters separately
const sequelize = new Sequelize('workload', 'sa', '123456', {
  host: 'localhost',
  dialect: 'mssql'/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

// sequelize
//   .authenticate()
//   .then(() => {
//     console.log('Connection has been established successfully.');
//   })
//   .catch(err => {
//     console.error('Unable to connect to the database:', err);
//   });
module.exports=sequelize;
