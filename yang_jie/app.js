'use strict'
var info = require('./info');
 info();

var table = require('./table')
var Crawler = require("crawler");
for (let index=0;index<=250;index+=25) {
    var URL ='https://book.douban.com/top250?start='+index
var c = new Crawler({
    maxConnections: 5000,
    // This will be called for each crawled page
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            var $ = res.$;
            var list = $('#wrapper .article table').toArray();
            list.forEach(element => {
                var txt1 = $(element).find('.pl2').text().trim().replace(/[ \n\r]/g, "");
                var txt2 = $(element).find('.pl').text().trim().replace(/\s*/g, "");
                var txt3 = $(element).find('.quote').text().trim().replace(/\s*/g, "");

                var edm = txt2.split('/').slice(0, 1).toString();
                table.create({ title: txt1, author: edm, information: txt3 })
            });
        }
        done();
    }
});

c.queue(URL);


}