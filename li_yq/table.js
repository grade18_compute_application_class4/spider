'use strict'
//引用
var Sequelize = require('sequelize');
//引用db路径
var sequelize = require('./db')


const Table = sequelize.define('table', {
  // attributes
  title: {
    type: Sequelize.STRING(),
    allowNull: false
  },
  who: {
    type: Sequelize.STRING(),
    // allowNull defaults to true
  },
  intro:{
    type: Sequelize.STRING(),
    allowNull: true
  }
  
}, {
  // options
});
module.exports=Table;