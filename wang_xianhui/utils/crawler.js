'use strict'
var User = require('../models/articles');
var Crawler = require('crawler');

var crawler = new Crawler({
    // 在每个请求处理完毕后将调用此回调函数
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            //将爬好的数据赋值给juqery对象
            var $ = res.$;
            // $ 默认为 Cheerio 解析器
            // 它是核心jQuery的精简实现，可以按照jQuery选择器语法快速提取DOM元素
            console.log($("title").text()); //获取标题
            // console.log(res.$('title').text());
            // console.log($("html").html());
            //获取要爬取的节点信息
            var list = $('#content .article .indent table').toArray(); //转为数组

            list.forEach(element => {
                //获取书名
                let bookname = $(element).find('.pl2 a').text().trim().replace(/[ \n]/g, ''); //替换正则
                //获取作者
                let author = $(element).find('p.pl').text().replace(/([/][^/]+){3,4}$/, '');
                //获取引文
                let quote = $(element).find('span.inq').text();

                User.Book.sync({
                    force: true
                }).then(() => {
                    User.Book.create({
                        BookName: bookname,
                        Author: author,
                        Quote: quote
                    }).then(row => {
                        console.log(`所插入数据的ID：${row.id}:${row.BookName}:${row.Author}:${row.Quote}`)
                    });
                })


            });

        }
        done();
    }
});



// 写入需要爬取网页的地址
// crawler.queue(['https://book.douban.com/top250?start=0',
//     'https://book.douban.com/top250?start=25',
//     'https://book.douban.com/top250?start=50',
//     'https://book.douban.com/top250?start=75',
//     'https://book.douban.com/top250?start=100',
//     'https://book.douban.com/top250?start=125',
//     'https://book.douban.com/top250?start=150',
//     'https://book.douban.com/top250?start=175',
//     'https://book.douban.com/top250?start=200',
//     'https://book.douban.com/top250?start=225',
//     'https://book.douban.com/top250?start=250'
// ]);

for (let val = 0; val <= 250; val += 25) {
    let url = 'https://book.douban.com/top250?start=' + val;
    crawler.queue(url);
}





//创建爬虫
// var crawler = new Crawler({
//     maxConnections: 10, //最大连接数
//     //这将调用每个抓取的页面
//     callback: function (error, res, done) {
//         //爬取完成后回调用这个函数
//         if (error) {
//             console.log(`爬取错误：${error}`);
//         } else {
//             //将爬取好的数据赋予给jquery
//             var $ = res.$;
//         }
//     }
// })

// var c = new Crawler({
//     maxConnections: 10,
//     // This will be called for each crawled page
//     callback: function (error, res, done) {
//         //爬好之后会执行这个回调函数
//         if (error) {
//             console.log(error);
//         } else {
//             //将爬好的数据赋值给juqery对象
//             var $ = res.$;
//             // $ is Cheerio by default
//             //a lean implementation of core jQuery designed specifically for the server
//             // 爬取页面结构dom树
//             console.log($("html").html());
//             // 爬取页面的图片
//             console.log($('#lg > img').attr('src'));
//         }
//         done();
//     }
// });

//https://book.douban.com/top250?icn=index-book250-all