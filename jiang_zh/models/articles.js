'use strict'
var Sequelize = require('sequelize');
var sequelize = require('../db');
const table = sequelize.define('articles', {
    title: {//标题
        type: Sequelize.STRING(1000),
        allowNull: false
    },author:{//作者
        type:Sequelize.STRING(1000),
        allowNull:false
    },Bookreview:{//书评
        type:Sequelize.STRING(1000),
        allowNull:false
    }
}, {
    //options
});
module.exports = table;