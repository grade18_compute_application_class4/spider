
var Sequelize=require('sequelize');
//引用路径
var sequelize=require('./model')

const Model = Sequelize.Model;
class Table extends Model {}
Table.init({
  // attributes
  bookName: {
    type: Sequelize.STRING,
    allowNull: false
  },
  author: {
    type: Sequelize.STRING
    // allowNull defaults to true
  },
  introduce:
  {
      type:Sequelize.STRING
  }

}, {
  sequelize,
  modelName: 'table'
  // options
});
module.exports=Table;