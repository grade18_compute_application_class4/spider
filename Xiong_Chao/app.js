'use strict'
const fs = require('fs')
const request = require('request')
var insert = require('./sql/insert');
insert();
var table = require('.//createTable/table');
var Crawler = require("crawler");
var c = new Crawler({
    rateLimit:200 ,
    maxConnections: 1,
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            var $ = res.$;

            var list = $('#content .article table').toArray();
            list.forEach(element => {
                var txt0 = $(element).find(' a ').text().trim().replace(/\s*/g, "");

                var txt1 = $(element).find(' .pl ').text().trim().replace(/\s*/g, "");
                var a = txt1.split('/').slice(0,1).toString();

                var txt2 = $(element).find(' .inq ').text().trim().replace(/\s*/g, "");

                var txt3 = $(element).find(' img ').attr('src');
                var imgName = txt3.split('/').slice(7,8).toString();
                request(txt3).pipe(fs.createWriteStream('./img/'  + imgName + '.jpg'))

                table.create({ txtname: txt0, artname: a, introduced: txt2 ,picture:txt3});
            });
        }
        done();
    }
});

for (let index = 0; index < 250; index += 25) {
    c.queue('https://book.douban.com/top250?start='+index);     
}