'use strict'

var articles = require('../models/articles')

module.exports = () => {
    articles.sync({ force: true });
}
