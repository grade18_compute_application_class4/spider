'use strt'

var Sequelize = require('sequelize');
var sequelize = require('../db')

const table = sequelize.define('articles', {
    // attributes
    title: {
        type: Sequelize.STRING
    },

    Author: {
      type: Sequelize.STRING  
    },

    Introduction: {
      type: Sequelize.STRING 
    }
}, {
    // options
});

module.exports = table;