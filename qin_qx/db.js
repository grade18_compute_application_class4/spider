'use strict'

const Sequelize = require('sequelize');

// Option 2: Passing a connection URI
const sequelize = new Sequelize('mssql://sa:123456@localhost:1433/NodeJsMssql');

module.exports = sequelize;