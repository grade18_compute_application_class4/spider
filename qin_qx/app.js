'use strict'
const fs = require('fs')

const request = require('request')

var initDb = require('./utils/initDb');
initDb();

var articles = require('./models/articles');

var Crawler = require("crawler");

var c = new Crawler({
    // 这个回调每个爬取到的页面都会触发
    maxConnections: 10,

    callback: function (error, res, done) {
        if (error) {

            console.log(error);

        } else {
            // var $ = res.$;

            var list = res.$('#content .article table').toArray();

            list.forEach(element => {
                //用JQuery和正则表达式获取标题
                var title = res.$(element).find('.pl2 a').text().replace(/[ \n\r]/g, '');

                //用JQuery和正则表达式获取图片地址
                var img = res.$(element).find('.nbg > img').attr('src');

                //用JQuery和正则表达式获取作者
                var author = res.$(element).find('.pl').text().replace(/[ \n\r]/g, '').replace(/[/].*$/, '');

                //用JQuery和正则表达式获取简介
                var introduction = res.$(element).find('.quote .inq').text().replace(/[ \n\r]/g, '');

                //把爬取到的数据写入数据库中
                articles.create({ title: title, Author: author, Introduction: introduction })

                //控制时间间隔尽量保持图片下载完整
                setTimeout(function () {

                    //通过流的方式，把图片保存到/img目录下，并用标题作为图片的名称
                    request(img).pipe(fs.createWriteStream('./img/' + title + '.jpg'));

                }, 5000);

            });
        }
        done();
    }
});

// 寻找规律并循环爬取URL，使用默认的callback
for (var a = 0; a <= 225; a += 25) {

    var url = 'https://book.douban.com/top250?start=' + a

    c.queue(url);
}

