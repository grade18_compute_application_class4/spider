'use strict'

var Sequelize = require('sequelize');
var sequelize = require('../db');

var wrapper = sequelize.define('wrapper', {
    title: {
        type: Sequelize.STRING(4000),
        allowNull: false
    },
    artName: {
        type: Sequelize.STRING(4000),
        allowNull: false
    },
    time: {
        type: Sequelize.DATE,
        allowNull: false
    }
});

module.exports = wrapper;