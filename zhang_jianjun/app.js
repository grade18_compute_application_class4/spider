// 'use strict'

var insert = require('./InsertSql/insert');
insert();
var table = require('./module/table');

var c = new Crawler({
    rateLimit:10,
    maxConnections: 1,
    // This will be called for each crawled page
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            var $ = res.$;

            var list = $('#wrapper .post_item_body').toArray();
            list.forEach(element => {
                var txt0 = $(element).find(' .titlelnk ').text().trim().replace(/\s*/g, "");
                var txt1 = $(element).find(' .lightblue ').text().trim().replace(/\s*/g, "");
                var txt2 = $(element).find(' .post_item_foot ').text();
                
                var createTime = txt2.match(/\d{4}-\d{2}-\d{2} \d{2}:\d{2}/).toString();

                table.create({ title: txt0, artName: txt1, time: createTime});
                // console.log(createTime);
            });
        }
        done();
    }
});

for (let index = 1; index <= 200; index++) {
    var a = 'sitehome/p/'+index;
    c.queue('https://www.cnblogs.com/'+a);
}

   



