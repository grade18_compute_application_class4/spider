'use strict'

const sequelize = require('../db');
const Sequelize = require('sequelize');

const book = sequelize.define('crabook_1', {
    title: {
        type: Sequelize.STRING(80),
        allowNull: false
    },
    author: {
        type: Sequelize.STRING(80),
        //allowNull: false
    },
    price: {
        type: Sequelize.STRING,
    },
    quote: {
        type: Sequelize.STRING(500),
    }
});

module.exports=book;