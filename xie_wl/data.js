'use strict'
// 爬取数据

const Crawler = require('crawler');
const Book = require('./model/booK');

// var url = 'https://book.douban.com/top250?start=0';
// var startPage = 0;
// var endPage = 225;
// var arr = [0, 25, 50, 75, 100, 125, 150, 175, 200, 225];
// var array = [];
// var int = 1;
// var xls = 0;

const crawler = new Crawler({
    maxConnections: 100,
    callback: function (error, res, done) {
        if (error) {
            console.log(`错误信息：${error}`);
        } else {
            var $ = res.$;
            //查找类似jquery的
            // $ is Cheerio by default $是默认的Cheerio
            //a lean implementation of core jQuery designed specifically for the serve
            ////专门为服务设计的核心jQuery的精益实现
            var list = $('#content .article .indent table').toArray();
            //console.log($('title').text());
                list.forEach(element => {
                    var txt = element; //=>对象 jquery对象
                    //标题
                    var title = $(txt).find('.pl2 a').text().trim().replace(/[ \n\r]/g, '');
                    //作者
                    var author = $(txt).find('p.pl').text().replace(/([/][^/]+){3,4}$/, '')
                    //价格
                    var price = $(txt).find('p.pl').text().match(/([^/]+)$/)[1].replace(/([^\d]+)$/, '');
                    //引文
                    var quote = $(txt).find('span.inq').text()
                    // console.log(author);
                    // console.log(price);
                    // console.log(quote);
                    // Book.sync({ force: true }).then(() => {
                        Book.create({
                            title: `《${title}》`,
                            author: author,
                            price: price,
                            quote: quote
                        }).then((row) => {
                            console.log(`添加的记录Id:${row.id}`)
                        })
                    // })
                }); 
               // crawler.queue(url);
                //console.log(xls)
        }
        done();
    }
});

//crawler.queue('https://book.douban.com/top250?start=0')

module.exports=(page)=>{
    crawler.queue(page);
}
// module.exports=crawler;