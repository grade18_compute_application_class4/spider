'use strict'
// 链接数据库，测试数据库链接是否成功

var config=require('./config');
var Sequelize=require('sequelize');

const sequelize=new Sequelize(config.database,config.userName,config.password,{
    host:config.host,
    dialect:config.dialect
});
//authenticate
sequelize.authenticate().then(()=>{
    console.log(`链接数据库${config.database}成功！`)
}).catch((err)=>{
    console.log(`链接失败：${err}`);
});

module.exports=sequelize;