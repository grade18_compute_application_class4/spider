'use strict'

var init=require('./initDb');
init();

var tale=require('./table');
var Crawler=require("crawler");
for (let i=0;i<=250;i+=25) {
    var url ='https://book.douban.com/top250?start='+i
var a = new Crawler({
    maxConnections: 5000,
    // This will be called for each crawled page
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            var $ = res.$;
            var list = $('#wrapper .article table').toArray();
            list.forEach(element => {
                var txt1 = $(element).find('.pl2').text().trim().replace(/[ \n\r]/g, "");
                var txt2 = $(element).find('.pl').text().trim().replace(/[ \n\r]/g, "");
                var txt3 = $(element).find('.quote').text().trim().replace(/[ \n\r]/g, "");

                var a = txt2.split('/').slice(0, 1).toString();
                tale.create({ title: txt1, who: a, intro: txt3 })
                console.log(txt1);
                console.log(txt2);
                console.log(txt3);
            });
            // $ is Cheerio by default
            //a lean implementation of core jQuery designed specifically for the server

        }
        done();
    }
});

a.queue(url);


}