'use strict'
var Sequelize=require('sequelize');

var sequelize=require('./db');

var Users=sequelize.define('users',{
    title:{
        type:Sequelize.STRING(),
        allowNull:false
    },
    who:{
        type:Sequelize.STRING(),
    },
    intro:{
        type:Sequelize.STRING(),
        allowNull:true
    }
});
module.exports=Users;